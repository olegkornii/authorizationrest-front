import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import {Human} from '../models/human.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  board: string;
  errorMessage: string;
  private usersURL = 'http://localhost:8080/api/users';
  humans: Human[];
  constructor(private userService: UserService) { }
  deleteUser(user: Human): void {
    this.userService.deleteUser(user)
      .subscribe( data => {
        this.humans = this.humans.filter(u => u !== user);
      });
  }
  ngOnInit() {
    this.userService.getAdminBoard().subscribe(
      data => {
        this.board = data;
      },
      error => {
        this.errorMessage = `${error.status}: ${JSON.parse(error.error).message}`;
      }
    );
    this.userService.getUsers()
      .subscribe( data => {
        this.humans = data;
      });
  }
}
