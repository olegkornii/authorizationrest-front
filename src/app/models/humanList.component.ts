import {Component, Input, Output} from '@angular/core';
import {Human} from './human.model';

@Component({
  selector: 'app-human-list',
})
export class HumanListComponent {
   humans: Human[];
  constructor(humans: Human[]) {
    this.humans = humans;
  }
  @Output() getHumans() {
    return this.humans;
  }
  @Input() setHumans(humans: Human[]) {
    this.humans = humans;
  }
}
