import {Component} from '@angular/core';
import {Human} from './human.model';

@Component({
  selector: 'app-human',
})
export class HumanComponent {
   human: Human;
  constructor(human: Human) {
    this.human = human;
  }
  getHuman() {
    return this.human;
  }
  setHuman(human: Human) {
    this.human = human;
  }
}
